![Python Versions](https://img.shields.io/badge/python-3.6%20%7C%203.7%20%7C%203.8%20%7C%203.9-blue) 
![Style Black](https://warehouse-camo.ingress.cmh1.psfhosted.org/fbfdc7754183ecf079bc71ddeabaf88f6cbc5c00/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f636f64652532307374796c652d626c61636b2d3030303030302e737667) 
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
![Test](https://github.com/MQSchleich/continuous-template/actions/workflows/python-app.yaml/badge.svg?branch=main)


# continious-template
A template for rapid, agile or lean development of python applications using the GitHub CI 
# Check how to write decent PyPi packages 
https://github.com/judy2k/publishing_python_packages_talk
